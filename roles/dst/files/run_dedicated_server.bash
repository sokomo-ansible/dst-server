#!/usr/bin/env bash

_CLUSTER_NAME="$1"
_CLUSTER_TYPE="$2"

#steamcmd_dir="$HOME/steamcmd"  #This is unnecessary if you set the environment variables for the steam user correctly as outlined earlier
_DST_INSTALL_DIR="$HOME/steamapps/DST"
_DST_CONFIG_BASE_DIR="$HOME/.klei/DoNotStarveTogether" #This folder structure [/.klei/DoNotStarveTogether] is HARD CODED into their program, do not change
_CLUSTER_DST_DIR="${_DST_CONFIG_BASE_DIR}/${_CLUSTER_NAME}"

function fail()
{
    echo Error: "$@" >&2
    exit 1
}

function check_for_file()
{
    if [ ! -e "$1" ]; then
        fail "Missing file: $1"
    fi
}

#cd "$steamcmd_dir" || fail "Missing $steamcmd_dir directory!"

#check_for_file "steamcmd.sh" #This is unecessary if you set the environment variables for the steam user correctly as outlined earlier
check_for_file "${_CLUSTER_DST_DIR}/cluster.ini"
check_for_file "${_CLUSTER_DST_DIR}/cluster_token.txt"
check_for_file "${_CLUSTER_DST_DIR}/${_CLUSTER_TYPE}/server.ini"

check_for_file "${_DST_INSTALL_DIR}/bin64"

cd "${_DST_INSTALL_DIR}/bin64" || fail

run_shared=(./dontstarve_dedicated_server_nullrenderer_x64)
run_shared+=(-console)
run_shared+=(-cluster "${_CLUSTER_NAME}")
run_shared+=(-monitor_parent_process $$)

"${run_shared[@]}" -shard "${_CLUSTER_TYPE}"
